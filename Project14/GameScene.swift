//
//  GameScene.swift
//  Project14
//
//  Created by Ruben Dias on 17/04/2020.
//  Copyright © 2020 Ruben Dias. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    
    var slots = [WhackSlot]()
    
    let initialPopupTime = 0.85
    var popupTime = 0.85
    
    var gameRounds: SKLabelNode!
    let maxRounds = 60
    var numRounds = 1 {
        didSet {
            gameRounds.text = "Round \(numRounds) of \(maxRounds)"
        }
    }
    
    var highScore: SKLabelNode!
    var gameScore: SKLabelNode!
    var highscore = 0 {
        didSet {
            highScore.text = "High Score: \(highscore)"
        }
    }
    var score = 0 {
        didSet {
            gameScore.text = "Score: \(score)"
            if score >= highscore {
                highscore = score
            }
        }
    }
    
    var gameOver: SKSpriteNode!
    var newGame: SKLabelNode!
    
    override func didMove(to view: SKView) {
        let background = SKSpriteNode(imageNamed: "whackBackground")
        background.position = CGPoint(x: 512, y: 384)
        background.blendMode = .replace
        background.zPosition = -1
        addChild(background)

        gameScore = SKLabelNode(fontNamed: "Chalkduster")
        gameScore.text = "Score: 0"
        gameScore.position = CGPoint(x: 8, y: 12)
        gameScore.horizontalAlignmentMode = .left
        gameScore.fontSize = 36
        addChild(gameScore)

        highScore = SKLabelNode(fontNamed: "Chalkduster")
        highScore.text = "High Score: 0"
        highScore.position = CGPoint(x: 1024 - 8, y: 12)
        highScore.horizontalAlignmentMode = .right
        highScore.fontSize = 36
        addChild(highScore)
        
        gameRounds = SKLabelNode(fontNamed: "Chalkduster")
        gameRounds.text = "Round \(numRounds) of \(maxRounds)"
        gameRounds.position = CGPoint(x: 8, y: 736)
        gameRounds.horizontalAlignmentMode = .left
        gameRounds.fontSize = 20
        addChild(gameRounds)
        
        gameOver = SKSpriteNode(imageNamed: "gameOver")
        gameOver.position = CGPoint(x: 512, y: 384)
        gameOver.zPosition = 1
        
        newGame = SKLabelNode(fontNamed: "Marker Felt")
        newGame.text = "Play Again"
        newGame.position = CGPoint(x: 512, y: 300)
        newGame.zPosition = 1
        newGame.horizontalAlignmentMode = .center
        newGame.fontSize = 38
        
        for i in 0 ..< 5 { createSlot(at: CGPoint(x: 100 + (i * 170), y: 410)) }
        for i in 0 ..< 4 { createSlot(at: CGPoint(x: 180 + (i * 170), y: 320)) }
        for i in 0 ..< 5 { createSlot(at: CGPoint(x: 100 + (i * 170), y: 230)) }
        for i in 0 ..< 4 { createSlot(at: CGPoint(x: 180 + (i * 170), y: 140)) }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
            self?.createEnemy()
        }
    }
    
    func createSlot(at position: CGPoint) {
        let slot = WhackSlot()
        slot.configure(at: position)
        addChild(slot)
        slots.append(slot)
    }
    
    func createEnemy() {
        if numRounds >= maxRounds {
            for slot in slots {
                slot.hide()
            }

            addChild(gameOver)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) { [weak self] in
                guard let self = self else { return }
                self.addChild(self.newGame)
            }

            return
        }
        
        numRounds += 1
        popupTime *= 0.991

        slots.shuffle()
        slots[0].show(hideTime: popupTime)

        if Int.random(in: 0...12) > 3 { slots[1].show(hideTime: popupTime) }
        if Int.random(in: 0...12) > 7 {  slots[2].show(hideTime: popupTime) }
        if Int.random(in: 0...12) > 9 { slots[3].show(hideTime: popupTime) }
        if Int.random(in: 0...12) > 10 { slots[4].show(hideTime: popupTime)  }

        let minDelay = popupTime / 2.0
        let maxDelay = popupTime * 2
        let delay = Double.random(in: minDelay...maxDelay)

        DispatchQueue.main.asyncAfter(deadline: .now() + delay) { [weak self] in
            self?.createEnemy()
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        let location = touch.location(in: self)
        let tappedNodes = nodes(at: location)

        for node in tappedNodes {
            if let labelNode = node as? SKLabelNode, labelNode.text == "Play Again" {
                score = 0
                numRounds = 1
                
                popupTime = initialPopupTime
                
                gameOver.removeFromParent()
                newGame.removeFromParent()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
                    self?.createEnemy()
                }
                
                continue
            }
            
            guard let whackSlot = node.parent?.parent as? WhackSlot else { continue }
            if !whackSlot.isVisible { continue }
            if whackSlot.isHit { continue }
            whackSlot.hit()
            
            if node.name == "charFriend" {
                score -= 5
                
                run(SKAction.playSoundFileNamed("whackBad.caf", waitForCompletion: false))
            } else if node.name == "charEnemy" {
                whackSlot.charNode.xScale = 0.85
                whackSlot.charNode.yScale = 0.85
                score += 1
                
                run(SKAction.playSoundFileNamed("whack.caf", waitForCompletion: false))
            }
        }
    }
}
